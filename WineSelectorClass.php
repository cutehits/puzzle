<?php
/**
 * 
 * @category   WineSelector Class
 * @package    Core Puzzle
 * @author     Chandra Shekhar Pandey<shekharmca2005@gmail.com>
 * @website    http://www.cutehits.com
 */
class WineSelector {

    private $outputfilename = "outputtest.txt"; // File Name that will contain the output.
    private $favourite_list_count = 5; // How many favourite list each person will provide
    private $max_size_buffer = 4096; //Buffer size to handle large files
    private $max_item_each_might_purchase = 3; // Max item each person might purchase

    /*
     * @Input: FileName: string
     * @Desc: This method will open file .
     * It will traverse whole file and will create a set of array with their person id and wine id.
     * Each array will contain a set of data that is provided for each person.
     *      
     */

    public function ExtractData($filename) {
        $handle = fopen($filename, "r");

        if ($handle) {
            $row_count = 1;
            while (($buffer = fgets($handle, $this->max_size_buffer)) !== false) {
                $data = explode("	", $buffer);
                $row_count++;
                $favourite_list_provided = $this->favourite_list_count + 1; // Because array will start from zero index
                if ($row_count % $favourite_list_provided == 0) { //Will create array of data per person
                    $this->AllowedPurchaseForEachPerson($per_person_favourite); // Pass per person data array for further validation 
                    unset($per_person_favourite); // Once first set of person has been done then flush that array so that we can consider next person
                } else {
                    $per_person_favourite[] = $data; // Creating per person array
                }
            }
            fclose($handle);
        }
    }

    /*
     * @Input: PerPersonArray: Array
     * @Desc: This will removed duplicate wine for per person.And clean array will be send for output file write.
     */

    public function AllowedPurchaseForEachPerson($PerPersonArray) {
        /* Recreating the array $data[1] contain wineid and $data[0] contain personid.
          The idea of recreating array so that per person no duplicate wine will get created */
        foreach ($PerPersonArray as $data) {
            $output_array[$data[1]] = $data[0];
        }
        $this->WriteInOutputFile($output_array);
    }

    /*
     * @Input: ArrayForOutput: Array
     * @Desc: It will write only max allowed item to be choosen for output file and other items will be discarded.
     */

    public function WriteInOutputFile($ArrayForOutput) {
        $file = fopen($this->outputfilename, "a");

        $i = 0;
        foreach ($ArrayForOutput as $key => $value) {
            $text = $value . " " . $key;
            if ($i < $this->max_item_each_might_purchase) { // Ensure that only certain items will be allowed to perchase.
                fwrite($file, $text);
            }
            $i++;
        }
        fclose($file);
    }

}

?>